Starting up the project

* Install Meteor
    
    Linux

    <pre>curl https://install.meteor.com/ | sh</pre>

    Windows

    <pre>choco install meteor</pre>

* Install npm packages

    <pre>npm install</pre>

* Running the solution locally

    <pre>meteor run</pre>
export interface Player {
    firstName: string
    lastName: string
    handicap?: number
}
import util from 'util';
import {navigateTo} from 'svelte-grouter'
import { UserDto } from "./UsersMethods";
import { Role } from "./Roles";

let meteorCallPromise = util.promisify(Meteor.call.bind(this));
let changePasswordPromise
if (Meteor.isClient) {
    changePasswordPromise = util.promisify(Accounts.changePassword.bind(this));
}
export function registerPlayer(user: UserDto) : Promise<string> {
    return meteorCallPromise('registerPlayer', user)
    .then(() => {
        Meteor.loginWithPassword(user.email, user.password, err =>  {
            if (err) {
                console.error(err)
            } else {
                navigateTo('myOutings')
            }
        })
    })
    .catch(console.error);
}
export function createUser(user: UserDto) : Promise<string> {
    return meteorCallPromise('createNewUser', user)
    .then(() => {
        navigateTo('myOutings')
    })
    .catch(console.error);
}

export function changePassword(currentPassword, newPassword) {
    return changePasswordPromise(currentPassword, newPassword)
    .catch(e => {
        console.error(e)
        throw e
    })
}


export function validate(user: UserDto): UserValidation {

    const invalidFields = {} as UserValidation;
    if (!user.firstName) {
        invalidFields.firstName = "Required"
    }
    if (!user.lastName) {
        invalidFields.lastName = "Required"
    }
    if (!user.email) {
        invalidFields.email = "Required"
    } else if (!user.email.match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i)) {
        invalidFields.email = 'Email must be valid'
    }
    if (!user.role) {
        invalidFields.role = "Required"
    } else if (!Object.values(Role).includes(user.role)) {
        invalidFields.role = "Role must be one of " + Object.values(Role)
    }
    return invalidFields;

}

// reason why the value is not valid.
export interface UserValidation {
    firstName?: string
    lastName?: string
    email?: string
    password?: string
    outingIds?: string
    role?: string
}
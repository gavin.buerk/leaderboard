import { Score } from "./Scores"
import util from 'util'

const meteorCallPromise = util.promisify(Meteor.call.bind(this))


export function reportScore(score: Score) : Promise<string> {
    return meteorCallPromise('reportScore', score)
    .catch(console.error)
}

export function validate(score: Score): ScoreValidation {
    const invalidFields = {} as ScoreValidation
    if (!score.courseName) {
        invalidFields.courseName = 'Required'
    } 
    if (!score.playerName) {
        invalidFields.playerName = 'Required'
    } 
    if (score.par === undefined) {
        invalidFields.par = 'Required'
    } else if (typeof score.par !== 'number') {
        invalidFields.par = 'Must be numeric'
    }
    if (score.score === undefined) {
        invalidFields.score = 'Required'
    } else if (typeof score.score !== 'number') {
        invalidFields.score = 'Must be numeric'
    }

    if (score.courseRating === undefined) {
        invalidFields.courseRating = 'Required'
    } else if (typeof score.courseRating !== 'number') {
        invalidFields.courseRating = 'Must be numeric'
    } else if (score.courseRating < 60 || score.courseRating > 100) {
        invalidFields.courseRating = 'Must be between 60 and 100'
    }

    if (score.courseSlope === undefined) {
        invalidFields.courseSlope = 'Required'
    } else if (typeof score.courseSlope !== 'number') {
        invalidFields.courseSlope = 'Must be numeric'
    } else if (score.courseSlope < 55 || score.courseSlope > 155) {
        invalidFields.courseSlope = 'Must be between 55 and 155'
    }
    
    if (score.handicap !== undefined && typeof score.score !== 'number') {
        invalidFields.handicap = 'Must be numeric'
    }
    return invalidFields
}

export interface ScoreValidation {
    courseName?: string
    playerName?: string
    score?: string
    par?: string
    courseRating?: string
    courseSlope?: string
    handicap?: string
}
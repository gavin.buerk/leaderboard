import { checkRoleThrowException, Role } from "./Roles";

if (Meteor.isServer) {
    Meteor.publish('userData', () => {
        return Meteor.users.find(Meteor.userId(), { 
            fields: { 
                outingIds: true, 
                firstName: true, 
                lastName: true 
            } as any 
        });
    })
    Meteor.publish('allUserData', () => {
        checkRoleThrowException(Role.ADMIN)
        return Meteor.users.find({}, {
            fields: {
                outingIds: true,
                firstName: true,
                lastName: true,
                emails: true
            } as any
        })
    })
}

if (Meteor.isClient) {
    Meteor.subscribe('userData')
}

interface Email {
    address: true
}
export interface User extends Meteor.User{
    _id: string,
    firstName: string,
    lastName: string,
    outingIds: string[]
}
export enum Role {
    ADMIN='admin',
    ORGANIZER='organizer',
    PLAYER='player'
}

export function checkRoleThrowException(roles: Role[] | Role) {
    if (!Array.isArray(roles)) {
        roles = [roles, Role.ADMIN] // Admin can do anything
    } else {
        roles = [...roles, Role.ADMIN]
    }
    if (!Meteor.userId() || !Roles.userIsInRole(Meteor.userId(), roles)) {
        throw new Meteor.Error('Unauthorized')
    }
}

if (Meteor.isServer) {
    Meteor.publish(null, function () {
        if (this.userId) {
            //@ts-ignore
            return Meteor.roleAssignment.find({ 'user._id': this.userId });
        } else {
            this.ready()
        }
    })
}
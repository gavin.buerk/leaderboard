import { Player } from "./Player";

export const Scores = new Mongo.Collection('Scores')

export interface Score {
    score: number
    courseRating: number
    courseSlope: number
    par: number
    playerName: string
    handicap?: number
    courseName: string
    outingId: string
}
import { Outing } from "./Outings";
import util from "util";

const meteorCallPromise = util.promisify(Meteor.call.bind(this));

export function createOuting(outing: Outing): Promise<string> {
  return meteorCallPromise("createOuting", outing).catch(console.error);
}

export function sendInvitation(outing: Outing, email: string): Promise<void> {
  return meteorCallPromise('sendInvitation', outing, email)
}

export function validate(outing: Outing): OutingValidation {
  const invalidFields = {} as OutingValidation;
  if (!outing.name) {
    invalidFields.name = "Required";
  }
  if (!outing.host) {
    invalidFields.host = "Required";
  }
  if (outing.startDate === undefined) {
    invalidFields.startDate = "Required";
  } else if (!(outing.startDate instanceof Date)) {
    invalidFields.startDate = "Must be a valid date";
  }
  if (outing.endDate === undefined) {
    invalidFields.endDate = "Required";
  } else if (!(outing.endDate instanceof Date)) {
    invalidFields.endDate = "Must be a valid date";
  }
  return invalidFields;
}

export interface OutingValidation {
  name?: string;
  host?: string;
  startDate?: string;
  endDate?: string;
}

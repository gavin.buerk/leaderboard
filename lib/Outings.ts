import { Score } from "./Scores";

export const Outings = new Mongo.Collection('Outings')

export interface Outing {
    _id: string,
    name: string
    host: string
    startDate: Date
    endDate: Date
}
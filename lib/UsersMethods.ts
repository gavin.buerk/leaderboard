import * as usersService from './UsersService'
import { Role } from "./Roles"
import { Random } from 'meteor/random'

if (Meteor.isServer) {
    Meteor.methods({
        registerPlayer(user: UserDto): string {
            const invalidFields = usersService.validate(user)
            if (Object.keys(invalidFields).length > 0) {
              throw new Meteor.Error(
                "Error validating User",
                JSON.stringify(invalidFields)
              )
            }

            const playerId = Accounts.createUser({email:user.email, password:user.password})
            Roles.addUsersToRoles(playerId, Role.PLAYER)
            Meteor.users.update(playerId, {$set: {firstName: user.firstName, lastName:user.lastName, outingIds:user.outingIds}})
    
            console.log(playerId);
            return playerId;
        },
        createNewUser(user: UserDto): string {
            const invalidFields = usersService.validate(user)
            if (Object.keys(invalidFields).length > 0) {
              throw new Meteor.Error(
                "Error validating User",
                JSON.stringify(invalidFields)
              )
            }

            console.log(user)

            const generatedPassword = Random.id()
            //@ts-ignore
            const userId = Accounts.createUser({email:user.email, password: generatedPassword})
            Roles.addUsersToRoles(userId, user.role)
            Meteor.users.update(userId, {$set: {firstName: user.firstName, lastName:user.lastName}})

            // TODO: Send an email to the new user with the generated password and the link to reset / get started

            // Email.send({
            //     text: {
            //         "..." + generatedPassword
            //     }
            // })
            return userId
        }
    })
}

export interface UserDto {
    _id?: string
    firstName: string
    lastName: string
    outingIds?: string[]
    email: string
    password: string
    role?: Role
}
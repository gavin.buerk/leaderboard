import { Score, Scores } from "./Scores"
import * as scoresService from './ScoresService'
import { Role, checkRoleThrowException } from "./Roles"

Meteor.methods({
    reportScore(score: Score): string {
        checkRoleThrowException([Role.PLAYER, Role.ORGANIZER])
        const invalidFields = scoresService.validate(score)
        if (Object.keys(invalidFields).length > 0 ) {
            throw new Meteor.Error('Error validating Score', JSON.stringify(invalidFields))
        }
        return Scores.insert(score)
    }
})
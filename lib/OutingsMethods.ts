import { Outing, Outings } from "./Outings"
import * as outingsService from "./OutingsService"
import { Role, checkRoleThrowException } from "./Roles"
import { User } from "./Users"
let invitationEmailTemplate
if (Meteor.isServer) {
  //@ts-ignore - This is really strange, I know, but it's a thing with Meteor.  
  //You can execute an import only the server, not the client, which is what we want here
  import InvitationEmail from '/server/InvitationEmail'
  invitationEmailTemplate = InvitationEmail
}

Meteor.methods({
  createOuting(outing: Outing): string {
    checkRoleThrowException(Role.ORGANIZER)
    const invalidFields = outingsService.validate(outing)
    if (Object.keys(invalidFields).length > 0) {
      throw new Meteor.Error(
        "Error validating Outing",
        JSON.stringify(invalidFields)
      )
    }
    const outingId = Outings.insert(outing)
    Meteor.users.update(Meteor.userId(), {$push: {outingIds: outingId}})
    return outingId
  },
  sendInvitation(outing: Outing, email: string): void {
    checkRoleThrowException(Role.ORGANIZER)
    if (Meteor.isServer) {
      console.log('About to search for ' + email)
      const user = Accounts.findUserByEmail(email) as User
      const message = invitationEmailTemplate.render({email, outing, user})
      Email.send({
        from: 'welcome@TODO.com', 
        to:email, 
        subject: user ? 'You have been invited to a new outing on TODO!' : 'Welcome to TODO!', 
        html: message.html
      })

      if (user) {
        Meteor.users.update(user._id, {$push: {outingIds: outing._id}})
      }
    }
  }
})

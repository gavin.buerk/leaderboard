import { Outings } from "../lib/Outings"
import { User } from "../lib/Users"
import { Role, checkRoleThrowException } from "../lib/Roles"

Meteor.publish('outingsByUser', () => {
    const user = Meteor.user() as User
    if (Roles.userIsInRole(user, Role.ADMIN))
        return Outings.find()
    else
        return Outings.find({ _id: { $in: user.outingIds } })
})
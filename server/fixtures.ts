import { Scores, Score } from "../lib/Scores"
import { Outing, Outings } from "../lib/Outings"
import { Role } from "../lib/Roles"

Meteor.startup(() => {
    // createOutings()
    // createScores()
    // createRoles()
    // createUsers()
    // refreshDB()
}) 

function refreshDB() {
    Scores.remove({})
    Outings.remove({})
    Meteor.users.update({}, {$set: {outingIds: []}}, {multi: true})
}

let outingIds = []

function createRoles() {
    Roles.createRole(Role.ADMIN);
    Roles.createRole(Role.ORGANIZER);
    Roles.createRole(Role.PLAYER);
}

function createUsers() {
    Meteor.users.remove({})
    const adminId = Accounts.createUser({email:'gavin.buerk@gmail.com', password:'Captech123!'})
    Roles.addUsersToRoles(adminId, Role.ADMIN)
    Meteor.users.update(adminId, {$set: {firstName: 'Gavin', lastName:'Buerk'}})

    const organizerId = Accounts.createUser({email:'gavin.buerk+organizer@gmail.com', password:'Captech123!'})
    Roles.addUsersToRoles(organizerId, Role.ORGANIZER)
    Meteor.users.update(organizerId, {$set: {firstName: 'Gavin', lastName:'Organizer'}})

    const playerId = Accounts.createUser({email:'gavin.buerk+player@gmail.com', password:'Captech123!'})
    Roles.addUsersToRoles(playerId, Role.PLAYER)
    Meteor.users.update(playerId, {$set: {firstName: 'Gavin', lastName:'Player', outingIds:outingIds}})
}

function createOutings() {
    const outings = [
        {
            name: 'Outing 1',
            host: '' // Will be a userID of a logged in user
        },
        {
            name: 'Outing 2',
            host: '' // Will be a userID of a logged in user
        },
        {
            name: 'Outing 3',
            host: '' // Will be a userID of a logged in user
        },
        {
            name: 'Outing 4',
            host: '' // Will be a userID of a logged in user
        },
    ] as Outing[]

    Outings.remove({})
    outings.forEach(outing => {
        outingIds.push(Outings.insert(outing))
    })
}

function createScores() {
    const scores = [
        {
            playerName: 'Cool Guy',
            score: 70,
            par: 72,
            outingId: 'ejsQJSwJzvCDyJv6b'
        },
        {
            playerName: 'Cool Guy 2',
            score: 69,
            par: 72,
            outingId: 'ejsQJSwJzvCDyJv6b'
        },
        {
            playerName: 'Cool Guy 3',
            score: 72,
            par: 72,
            outingId: 'ejsQJSwJzvCDyJv6b'
        },
    ] as Score[]

    Scores.remove({})
    scores.forEach(score => Scores.insert(score))
}

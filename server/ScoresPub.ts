import { Scores } from "../lib/Scores"

Meteor.publish('scores', (outingId: string) => {
    return Scores.find({outingId})
})
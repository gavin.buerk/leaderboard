import {readable} from 'svelte/store'
import { User } from '../lib/Users'

export const usersStore = readable([], set => {
    const computation = Tracker.autorun(() => {
        Meteor.subscribe('allUserData')
        set(Meteor.users.find().fetch())
    })
    return () => {
        computation.stop()
    }
})

export const currentUserStore = readable(undefined, set => {
    const computation = Tracker.autorun(() => {
        if ((Meteor.user() as User)?.firstName) {
            set(Meteor.user())
        }
    })
    return () => {
        computation.stop()
    }
})
import App from './App'

Meteor.startup(() => {
    new App({
        target: document.querySelector('body')
    })
})
export async function presentToast(message: string, duration = 4000) {
    const toast = document.createElement('ion-toast') as Toast
    toast.message = message
    toast.duration = duration
    document.body.appendChild(toast)
    return toast.present()
}

export interface Toast extends HTMLElement {
    message: string
    duration: number
    present: Function
}
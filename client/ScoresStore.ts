import {derived} from 'svelte/store'
import { Scores } from '../lib/Scores'
import {currentRoute} from 'svelte-grouter'

export const scoresStore = derived(currentRoute, ($currentRoute: any, set) => {
    if ($currentRoute.params.id) {
        const computation = Tracker.autorun(() => {
            Meteor.subscribe('scores', $currentRoute.params.id)
            set(Scores.find().fetch())
        })
        return () => {
            computation.stop()
        }
    } else {
        set([])
    }
})
import Leaderboard from '/client/pages/Leaderboard'
import ReportScore from '/client/pages/ReportScore'
import CreateOuting from '/client/pages/CreateOuting'
import Invite from '/client/pages/Invite'
import MyOutings from '/client/pages/MyOutings'
import ManageUsers from '/client/pages/ManageUsers'
import CreateUser from '/client/pages/CreateUser'
import ChangePassword from '/client/pages/ChangePassword'
import Register from '/client/pages/Register'
import SecuredLayout from '/client/layouts/SecuredLayout'
import PublicLayout from '/client/layouts/PublicLayout'
import { Role } from '../lib/Roles'

export const routes = {
    'leaderboard': {
        name: 'leaderboard',
        path: '/leaderboard/:id',
        layout: {
            component: SecuredLayout,
            content: Leaderboard,
            role: [Role.ORGANIZER, Role.PLAYER]
        }
    },
    'createOuting': {
        name: 'createOuting',
        path: '/createouting',
        layout: {
            component: SecuredLayout,
            content: CreateOuting,
            role: Role.ORGANIZER
        }
    },
    'invite': {
        name: 'invite',
        path: '/invite/:id',
        layout: {
            component: SecuredLayout,
            content: Invite,
            role: Role.ORGANIZER
        }
    },
    'publicLeaderboard': {
        name: 'publicLeaderboard',
        path: '/public/leaderboard/:id',
        layout: {
            component: PublicLayout,
            content: Leaderboard
        }
    },
    'myOutings': {
        name: 'myOutings',
        path: '/myoutings',
        layout: {
            component: SecuredLayout,
            content: MyOutings,
            role: [Role.PLAYER, Role.ORGANIZER]
        }
    },
    'manageUsers': {
        name: 'manageUsers',
        path: '/users',
        layout: {
            component: SecuredLayout,
            content: ManageUsers,
            role: Role.ADMIN
        }
    },
    'createUser': {
        name: 'createUser',
        path: '/createuser',
        layout: {
            component: SecuredLayout,
            content: CreateUser,
            role: Role.ADMIN
        }
    },
    'changePassword': {
        name: 'changePassword',
        path: '/changepassword',
        layout: {
            component: SecuredLayout,
            content: ChangePassword,
        }
    },
    'reportScore': {
        name: 'reportScore',
        path: '/report/:id',
        layout: {
            component: SecuredLayout,
            content: ReportScore,
            role: [Role.PLAYER, Role.ORGANIZER]
        }
    },
    'registerPlayer': {
        name: 'registerPlayer',
        path: '/register',
        layout: {
            component: PublicLayout,
            content: Register
        }
    },
    'default': {
        name: 'default',
        path: '/',
        redirectTo: 'myOutings'
    }
}
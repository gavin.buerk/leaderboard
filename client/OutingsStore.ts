import {readable, derived} from 'svelte/store'
import { Outings } from '../lib/Outings'
import { currentRoute } from 'svelte-grouter'

export const outingsByUserStore = readable([], set => {
    const computation = Tracker.autorun(() => {
        Meteor.subscribe('outingsByUser')
        set(Outings.find().fetch())
    })
    return () => {
        computation.stop()
    }
})

export const activeOutingStore = derived([currentRoute, outingsByUserStore], ([$currentRoute, $outingsByUserStore]: any[], set) => {
    if ($currentRoute.params.id && $outingsByUserStore) {
        set($outingsByUserStore.filter(outing => outing._id === $currentRoute.params.id)[0])
    } else {
        set({})
    }
})
# Requirements

Purpose:
     
    At a high level it is a Public Leaderboard for "Remote" Golf Outings.  In the era of COVID, large golf outings are no longer happening due to social distancing thus a new way to think about how an outing occurs is needed.  This Leaderboard application would allow for an individual or team to submit a score to a specific outing from any course and then a net/composite score would be calculated such to level the playing field.  Other features that could be included are: picture/video sharing, fundraising options, live hole-by-hole score entry, etc.  

Outing model:

    * Each hole score
    * handicap per hole
    * par per hole
    * -- OR --
    * total round score
    * course handicape (rating / slope)
    * Par of the course (70 - 72ish)
    * Name (individual name)
    * individual handicap - optional


Roles:

    * Sys Admin
        * can execute all functions / can do anything.
        * CRUD Outing Organizers
        * CRUD Players
        * CRUD Outings
        * View all Outings
        * View all Organizers
        * View all Players
    * Outing Organizer
        * CRUD Outings
        * See all history of previous outings
        * Organizers Invite players to outings. (Via Email address?, do we an STMP connection?)
        * Organizers can report scores to events for which they have organized.
    * Player
        * Join an Outing
        * CRUD Score from an Outing
        * See all history from Outings that were participated in
        * Players are Invited to Outings.
        * Players can only report scores to outings that they have been invited to.




Current TODO's

    * Gavin -> Routes & Login
    * Frankie -> Course Model
    * Stephen -> Outing Model & outing page & build out GitLab Task lists.
    * Allen -> OutingsService (client) & OutingsMethods (server) copy of ScoresService & ScroesMethods



* Default Homepage
    * Player
        * Nothing?
        * See all Public Outings


* Link with specific Outing

TODO: Gaivn investigate Email solution,
Stephen & Danny & Jessie -> requirements documentation & Process flow
Allen: Front End layout investigation? Bootstrap? Tailwinds? Bulma 


Non-MVP ideas

    Fundraising type items (super generic)
    Image upload of compelted scorecard
    Picture Upload